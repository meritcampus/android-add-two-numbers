package com.example.addtwonumbers;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	EditText firstNumber, secondNumber;
	Button addButton;
	TextView resultText;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_two_layout);
		firstNumber = (EditText) findViewById(R.id.first_edittext);
		secondNumber = (EditText) findViewById(R.id.second_edittext);
		addButton = (Button) findViewById(R.id.add_button);
		resultText = (TextView) findViewById(R.id.resultText);
		addButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.add_button:
			if(firstNumber.getText().toString().length() >0&&secondNumber.getText().toString().length() >0){
				resultText.setVisibility(View.VISIBLE);
			double firstValue = Double.parseDouble(firstNumber.getText().toString());
			double secondValue =  Double.parseDouble(secondNumber.getText().toString());
			double result =firstValue + secondValue;
			resultText.setText(Math.round(result)+"");
			}else{
				
				Toast.makeText(getApplicationContext(), "Fields should not be empty", Toast.LENGTH_SHORT).show();
			}

			break;

		default:
			break;
		}
	}

}
